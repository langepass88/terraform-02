resource "digitalocean_ssh_key" "user_ssh_pub_key" {
  name = "user_public_ssh_key"
  public_key = file(var.user_ssh_pub_key_path)
}

resource "digitalocean_tag" "devops" {
  name = var.devops_tag
}

resource "digitalocean_tag" "user_email" {
  name = var.user_email_tag
}

resource "digitalocean_droplet" "lb" {
  image = "ubuntu-20-04-x64"
  name = var.droplet_lb
  region = "nyc1"
  size = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.rebrain_ssh_pub_key.id, digitalocean_ssh_key.user_ssh_pub_key.fingerprint]
  tags = [digitalocean_tag.devops.id, digitalocean_tag.user_email.id]
}

resource "digitalocean_droplet" "app" {
  count = var.app_count
  image = "ubuntu-20-04-x64"
  name = "${var.droplet_app}-${count.index + 1}"
  region = "nyc1"
  size = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.rebrain_ssh_pub_key.id, digitalocean_ssh_key.user_ssh_pub_key.fingerprint]
  tags = [digitalocean_tag.devops.id, digitalocean_tag.user_email.id]
}

resource "aws_route53_record" "www_lb" {
  zone_id = data.aws_route53_zone.rebrain_zone.zone_id
  name    = "${var.droplet_lb}-${var.dns_name}"
  type    = "A"
  ttl     = 300
  records = [local.droplet_lb_ip]
}

resource "aws_route53_record" "www_app" {
  count = var.app_count
  zone_id = data.aws_route53_zone.rebrain_zone.zone_id
  name    = "${var.droplet_app}${count.index + 1}-${var.dns_name}"
  type    = "A"
  ttl     = 300
  records = [local.droplet_app_ip[count.index]]
}

resource "time_sleep" "wait_n_minutes" {
    create_duration = "${var.app_count + 1}m"
    depends_on = [
      aws_route53_record.www_lb,
      aws_route53_record.www_app
    ]
}

resource "local_file" "inventory" {
    content  = local.template_inv
    filename = "${path.module}/ansible/inventory/inventory.yml"
    depends_on = [time_sleep.wait_n_minutes]

#  provisioner "local-exec" {
#    command = "ansible-playbook ansible/nginx.yml"
#  }
}
