variable "do_token" {
	type = string
	sensitive = true
	description = "token for Digital Ocean"
}

variable "aws_access_key" {
  type = string
  sensitive = true
  description = "access token for AWS"
}

variable "aws_secret_key" {
  type = string
  sensitive = true
  description = "secret key for AWS"
}

variable "rebrain_ssh_pub_key_name" {
  type = string
  sensitive = true
  description = "public ssh key for rebrain"
}

variable "user_ssh_pub_key_path" {
  type = string
  sensitive = true
  description = "public ssh key for user"  
}

variable "user_email_tag" {
  type = string
  description = "tag by user email"  
}

variable "devops_tag" {
  type = string
  description = "tag by task number"  
}

variable "droplet_lb" {
	type = string
	description = "name of droplet with load balancer"
}

variable "droplet_app" {
  type = string
  description = "name of droplet with web application"
}

variable "dns_name" {
  type = string
  description = "your dns name"
}

variable "aws_hosted_zone_name" {
  type = string
  description = "hosted zone name of AWS"
}

variable "app_count" {
  type = number
  description = "number of app droplets"
}