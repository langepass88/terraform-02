# Роль по установке nginx
Данная роль настраивает nginx на двух VPS.
На первом VPS настраивается load balancer, который проксирует все запросы через proxy_pass + upstream на второй VPS с помощью nginx.
На второй VPS создается виртуальный хост с собственной страницей index.html (загружается в каталог виртуального хоста по адресу /var/www/html) на первом хосту.

Requirements
--------------
- сервер с установленной Debian-подобной операционной системой (например, Ubuntu 20.04 LTS);
- настроенный DNS на данный сервер (FQDN) и на его поддомены.

Role Variables
--------------
Ключевые переменные:
- FQDN-сервера, на котором происходит установка и настройка Nginx (расположен в inventory/inventory.yml)

Overriding configuration templates
--------------
В данной роли присутствуют шаблоны конфигураций nginx и html-страниц в папке templates. По собственному желанию, можно загрузить собственные файлы.
- vh.conf.j2 (конфиг для виртуального хоста)
- default.j2 (конфиг для настройки load balancer)
- index.html.j2 (html страница для 80 порта у домена)

Dependencies
------------
Переменные в файлах defaults/main.yml и vars/main.yml, также SSL-сертификаты из директории files/ssl зашифрованы с помощью Ansible Vault. Необходим будет доступ к ключу расшифровки, либо шифрование своих обственных переменных.

Playbooks
----------------
Для запуска полного плейбука необходимо запустить команду:

`ansible-playbook -i 'inventory/inventory.yml' -u root 'nginx.yml'  --vault-password-file=.vault_pass`

Для запуска отдельной роли необходимо запустить команду:

`ansible-playbook -i 'inventory/inventory.yml' -u root 'nginx.yml' -t conf  --vault-password-file=.vault_pass`

License
-------
MIT / BSD

Author Information
------------------
Эта роль была создана в 2024 году Романовым Сергеем в качестве практического задания к выполнению практикума [Devops by Rebrain](https://rebrainme.com/devops/)