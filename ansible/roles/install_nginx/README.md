# Роль по установке nginx

Данная роль устанавливает на ОС Linux актуальную из репозиториев версию Nginx и загружает на удаленный сервер дефолтный nginx-конфиг (nginx.conf), с заранее заданными переменными.

Requirements
--------------
- сервер с установленной Debian-подобной операционной системой (например, Ubuntu 20.04 LTS) или RedHat-подобной операционной системой (например, CentOS7);
- настроенный DNS на данный сервер (FQDN) и на его поддомены.

Role Variables
--------------
Ключевые переменные:
- FQDN-сервера, на котором происходит установка и настройка Nginx (расположен в inventory/inventory.yml)

Overriding configuration templates
--------------
В данной роли присутствуют шаблоны конфигураций nginx и html-страниц в папке templates. По собственному желанию, можно загрузить собственные файлы.
- nginx.conf.j2 (стандартный конфиг nginx)

Dependencies
------------
None.

Playbooks
----------------
Для запуска полного плейбука необходимо запустить команду:

`ansible-playbook -i 'inventory/inventory.yml' -u root 'nginx.yml'`

Для запуска отдельной роли необходимо запустить команду:

`ansible-playbook -i 'inventory/inventory.yml' -u root 'nginx.yml' -t install`

License
-------
MIT / BSD

Author Information
------------------
Эта роль была создана в 2024 году Романовым Сергеем в качестве практического задания к выполнению практикума [Devops by Rebrain](https://rebrainme.com/devops/)
