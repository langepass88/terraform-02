locals {
  droplet_lb_ip = digitalocean_droplet.lb.ipv4_address
  droplet_app_ip = digitalocean_droplet.app[*].ipv4_address
  template_inv = templatefile("templates/inventory.tpl", {
    dns_lb = aws_route53_record.www_lb.fqdn 
    dns_apps = aws_route53_record.www_app[*].fqdn
    droplet_lb = var.droplet_lb
    droplet_apps = var.droplet_app[*]
    })
}