output "lb" {
  value = {
        dns-name_lb = aws_route53_record.www_lb.fqdn
        ip = digitalocean_droplet.lb.ipv4_address
  }
}

output "apps" {
  value = {
     for index, droplet in digitalocean_droplet.app :
       droplet.name => {
        dns-name_app = aws_route53_record.www_app[index].fqdn
        ip = digitalocean_droplet.app[index].ipv4_address
    }
  }
}